package ru.rencredit;

import org.junit.Test;
import org.openqa.selenium.By;

public class FirstTest extends WebDriverSettings {

    @Test
    public void Test1() throws InterruptedException {

        driver.manage().window().maximize();

        driver.get("https://rencredit.ru/");

        Thread.sleep(2000);

        driver.executeScript("scroll(0, 200);");

        Thread.sleep(2000);

        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[2]/div/div[2]/div[3]/div[1]/a[1]")).click();

        driver.executeScript("scroll(0, 100);");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"section_1\"]/div/div/form/div[1]/div[2]/div/div[1]/label/span[1]"))
                .click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"section_1\"]/div/div/form/div[1]/div[3]/div[1]/div/label")).click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"section_1\"]/div/div/form/div[1]/div[3]/div[1]/div/label/input"))
                .sendKeys("100000");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"section_1\"]/div/div/form/div[1]/div[4]/div[3]/div[2]/div[3]")).click();

        Thread.sleep(4000);

    }

    @Test
    public void Test2() throws InterruptedException {

        driver.manage().window().maximize();

        driver.get("https://rencredit.ru/");

        Thread.sleep(2000);

        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[2]/div/div[2]/div[1]/div[1]/a[1]")).click();

        Thread.sleep(2000);

        driver.executeScript("scroll(0, 170);");

        Thread.sleep(1000);

        driver.findElement(By.xpath("/html/body/div[4]/div/div[2]/div[1]/div/div[2]/a")).click();

        Thread.sleep(2000);

        driver.executeScript("scroll(0, 1000);");

        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"t1\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"t1\"]")).sendKeys("Иванов");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"t2\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"t2\"]")).sendKeys("Иван");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"t4\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"t4\"]")).sendKeys("9123456789");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"t38\"]")).click();

        driver.findElement(By.xpath("//*[@id=\"t38\"]")).sendKeys("ivanov@mail.ru");

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"section_1\"]/div[2]/div/form/div[1]/div[4]/div/label/div")).click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"s2-styler\"]/div[1]/div[2]")).click();

        Thread.sleep(2000);

        driver.findElement(By.xpath("//*[@id=\"s2-styler\"]/div[2]/ul/li[2]")).click();

        Thread.sleep(4000);

    }

}